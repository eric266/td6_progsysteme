#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

float lecture(const char* fichier){
	//verifie qu'un fichier est passer en paramètre
	if(fichier==NULL)
	{
		printf("un argument requis");
		exit(1);
	}

	int val;
	float valeur;
	int taille=0;

	//verifie qu'on peut ouvrir le fichier
    if((val=open(fichier , O_RDONLY))==-1)
	{ perror("exemple:open");
	  exit(1);
	}

	//recupere la taille du fichier
	taille=lseek(val, 0, SEEK_END);

	//recupère les valeurs du fichiers
	if(read(val,&valeur,taille)==-1)
	{
		perror("exemple:read");
		exit(2);
	}

	close(val);
    return(valeur);
}
