#include<stdio.h>
#include<stdlib.h>

// prototypes des fonctions externes
float lecure(char* );
float cretionTab(float ,char*);
void tri(float*);
float min(float* );
float max(float* );
float moyenne(float* );
void affichage (char* , float , float , float );

char* espece_setosa = "setosa";
char* espece_versicolor = "versicolor";
char* espece_virginica = "virginica";
 
int main(int argc,char *argv[])
{
    float* tableau_valf = lecture(argv[1]); // stocke les valeurs du fichier dans un tableau
    float* tableau_setosa = creationTab(tableau_valf, setosa); // crée un tableau contenant les valeurs de l'espèce setosa    
    float* tableau_versicolor = creationTab(tableau_valf, versicolor); // crée un tableau contenant les valeurs de l'espèce versicolor   
    float* tableau_virginica = creationTab(tableau_valf, virginica); // crée un tableau contenant les valeurs de l'espèce virginica   
    tri(tableau_setosa);  // trie le tableau contenant les valeurs de l'espèce setosa  
    tri(tableau_versicolor); // trie le tableau contenant les valeurs de l'espèce versicolor  
    tri(tableau_virginica); // trie le tableau contenant les valeurs de l'espèce virginica  
    float min_setosa = min(tableau_setosa);  // définit la valeur minimum de l'espèce setosa
    float min_versicolor = min(tableau_versicolor); // définit la valeur minimum de l'espèce versicolor
    float min_virginica = min(tableau_virginica);   // définit la valeur minimum de l'espèce virginica
    float max_setosa = max(tableau_setosa);    // définit la valeur maximum de l'espèce setosa
    float max_versicolor = max(tableau_versicolor);    // définit la valeur maximum de l'espèce versicolor   
    float max_virginica = max(tableau_virginica);   // définit la valeur maximum de l'espèce virginica
    float moyenne_setosa = moyenne(tableau_setosa);    // définit la valeur moyenne de l'espèce setosa
    float moyenne_versicolor = moyenne(tableau_versicolor);   // définit la valeur moyenne de l'espèce versicolor
    float moyenne_virginica = moyenne(tableau_virginica);   // définit la valeur moyenne de l'espèce virginica
    affichage(espece_setosa, moyenne_setosa, max_setosa, min_setosa );   //affiche les informations de l'espèce setosa
    affichage(espece_versicolor, moyenne_versicolor, max_versicolor, min_versicolor );    //affiche les informations de l'espèce versicolor
    affichage(espece_virginica, moyenne_virginica, max_virginica, min_virginica );  //affiche les informations de l'espèce virginica
    return 0;
}
