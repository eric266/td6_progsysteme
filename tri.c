#include<stdio.h>
#include<stdlib.h>
#DEFINE taille_tableau 50

void tri(float* tableau)
{
    int passage = 0;
    bool permutation = true;
    int en_cours;
    
    while (permutation) {
        permutation = false; 
        passage ++;
        for (index=0;index<taille_tableau-passage;index++) {
            if (tableau[index]>tableau[index+1]){
                permutation = true; // si on va modifier un élément du tableau, on passe permutation en "true" (vrai)
                int temp = tableau[index]; // on copie tableau[index] dans temp pour pouvoir permuter tableau[index] et tableau[index+1]
                tableau[index] = tableau[index+1]; // tableau[index] devient tableau [index+1]
                tableau[index+1] = temp; // tableau[index+1] devient temp (l'ancienne valeur de tableau[index]
            }
        }
    }
}
